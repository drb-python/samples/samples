# Samples Folder
This project gathers set of samples for Drb usages.
It shows the simplicity of drb to access and retrieve data contents.

DRB exposes a set of interfaces described [here: https://drb-python.gitlab.io/drb](https://drb-python.gitlab.io/drb/).

__Samples include the following snippets:__

| Link to the sample                                                              | Description                                                                                                                                                                                                             |  
|---------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [sentinel_2 full access sample](jupyter/jupyter_notebook_read_img_jp2_s2.ipynb) | This sample browses inside Sentinel-2 dataset to display its content. It automatically recognize format and access inside xml, jp2k and others formats. Is also retrieves image implementation to show localized images |
| [Sentinel-5P NO2 concentration](jupyter/S5P_notebook-IT.ipynb)                  | This sample uses DRB to retrieve N02 concentration evolution during the 1st COVID-19 wave between February and March of year 2020, in Europe.                                                                           |

<br/>
<br/>

As a quick snippet, DBB can be used both by using resolver (or not) to let DRB retrieve the related implementation for a given URL:
```python
from drb.factory import DrbFactoryResolver
url = '/path/to/data/S5P_OFFL_L2__NO2____20200201T112236_20200201T130406_11933_01_010302_20200204T163837.nc'
s5p_node  = DrbFactoryResolver().create(url)
nc_node = s5p_node['root']['PRODUCT']
xarray = nc_node['variables'] \
                ['nitrogendioxide_tropospheric_column'].get_impl(xarray.DataArray)
...
```
This code shows how to automatically recognize s5p/netcdf data and browses inside as a dictionary to access datasets as xarray format. This code uses standard DRB API only without any skill of the hidden implementation used by the resolver. 

or we can use the netcdf implementation bypassing the resolver:
```python
from drb.utils.url_node import UrlNode
from drb_impl_netcdf import DrbNetcdfNode

url = '/path/to/data/S5P_OFFL_L2__NO2____20200201T112236_20200201T130406_11933_01_010302_20200204T163837.nc'
s5p_node = DrbNetcdfNode(UrlNode(url))
nc_node = s5p_node['root']['PRODUCT']
xarray = nc_node['variables'] \
                ['nitrogendioxide_tropospheric_column'].get_impl(xarray.DataArray)
...
```
This method requires the knowledge of [drb-impl-netcdf](https://pypi.org/project/drb-impl-netcdf) that implements [DRB node](https://drb-python.gitlab.io/drb/dev/api.html#drbnode) interface. In production mode, this usage can be preferred to the resolver implementation when the input format is known because the resolution of the node may takes time. 
